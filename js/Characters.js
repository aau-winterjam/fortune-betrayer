
var Characters = {


    init: function() {
        this.chars = [];

        this.chars.push({
            name: 'Generic White Guy',
            imageName: 'whiteguy',
            image: game.load.image('whiteguy', 'content/sprites/chars/char0.png')
        });

        // this.chars.push({
        //     name: 'Generic White Guy',
        //     imageName: 'whiteguy',
        //     image: game.load.image('whiteguy', 'content/sprites/chars/char0.png')
        // });
    },

    get: function(index) {
        return this.chars[index];
    }
}