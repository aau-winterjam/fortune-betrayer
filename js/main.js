
var game;
var game_text;

window.onload = function(e) {
    $.getJSON("./content/text/dialogs.json", function (json) {
        game_text = json;
        console.log(json);
    });

    game = new Phaser.Game(800, 600, Phaser.AUTO, '');

    game.state.add('StateTitleScreen', StateTitleScreen);
    game.state.add('StateMainScreen', StateMainScreen);
    game.state.add('StateMiniGameScreen', StateMiniGameScreen);
    game.state.start('StateTitleScreen');

    console.log(game);
}