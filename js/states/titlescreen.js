var StateTitleScreen = {

    preload: function () {
        console.log("title screen preloaded");
        game.load.image("cover", "content/sprites/cover.png");
        game.load.image("outside", "content/sprites/outside_grob.png");
        game.load.image("title", "content/sprites/title_raw.png");

    },

    create: function () {
        console.log("title screen created");

        //
        game.add.image(0, 0, 'outside');
        game.add.image(0, 0, 'title');
        //var cover = game.add.image(0, 0, 'cover');
        //game.add.tween(cover).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);

        game.input.onDown.add(this.fadeOut);

        // game.camera.onFadeComplete.add((e) => { console.log(e); });
        game.camera.onFadeComplete.add(this.fadeComplete);

    },

    update: function () {

    },

    fadeOut: function(e) {
        game.camera.fade(0x000000, 1000);
        console.log('fading');
    },

    fadeComplete: function(e) {
        game.state.start('StateMainScreen');
    }


}