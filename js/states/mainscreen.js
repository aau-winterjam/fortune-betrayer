
var StateMainScreen = {

    preload: function () {
        // console.log("title screen preloaded");
        game.load.image("mainbg", "content/sprites/mainscreen.png");

        game.load.image("mainbackground_back", "content/sprites/mainbackground_back_fx.png");
        game.load.image("john", "content/sprites/Character.png");
        game.load.image("elf", "content/sprites/Character3.png");
        game.load.image("girl", "content/sprites/Character2.png");
        // game.load.image("title", "content/sprites/title_raw.png");

        Characters.init();

        this.texts = [];
        this.textStartY;
        this.stateText;
        this.character;

    },

    create: function () {
        // console.log("title screen created");
        //
        this.stateText = "Intro";

        var mainbg = game.add.image(0, 0, 'mainbg');
        //game.add.tween(mainbg).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(this.onFadeComplete);


        //this.backgroundBack = game.add.image(0, 0, 'mainbackground_back');
        //this.backgroundFore = game.add.image(0, 0, 'mainbackground_fore');
        this.character = this.game.add.image(5,5,'john');
        this.character.scale.setTo(0.5);

        this.textStartY = this.game.world.centerY;

        //var style = { font: "32px Arial", fill: "#ffffff"};
        //text = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "Hello again!", style);
        this.createTexts();
        this.quest_labe;

    },

    update: function () {
        // console.log("hello form title screen");

        for(var i = 0; i < this.texts.length; i++){
            if(this.texts[i].input.pointerOver()){
                this.texts[i].alpha = 1;
            }else{
                this.texts[i].alpha = 0.9;
            }
        }

    },

    onFadeComplete: function () {
        this.character = game.add.image(0, 90, Characters.get(0).imageName);
        // game.world.swap(this.character, this.backgroundFore);
    },

    createTexts: function () {
        var arr = [];
        var quest = "";
        let char_text = "";
        var style_quest = { font: "bold 16px Arial", fill: "#ffffff", wordWrap: true, wordWrapWidth:450,backgroundColor: "#c0c0c0c0"};
        var style_answer = { font: "16px Arial", fill: "#ffffff", wordWrap: true, wordWrapWidth:450,backgroundColor: "#c0c0c0c0"};
        for(var i = 0; i < game_text.passages.length; i++){


           if(game_text.passages[i].name === this.stateText){
               arr = game_text.passages[i].links;
               quest = game_text.passages[i].text;
               char_text = game_text.passages[i].name;
           }
        }

        var idx = quest.indexOf("[");
        quest = quest.substr(0,idx);
        //quest.replaceAll(/["']/g, "");


        this.quest_label = this.game.add.text(this.game.world.centerX,this.game.world.centerY, quest, style_quest);
        this.quest_label.anchor.set(0.5);
        this.quest_label.setShadow(5,5,'rgba(0,0,0,0.5)',15);

        let prevText;
        for(var i = 0; i < arr.length; i++){

            let route = "A" + (i+1) + ": " + arr[i].name;
            let link = arr[i].link;

            var tmp = this.quest_label.y;
            tmp = tmp + (this.quest_label.height / 2);
            tmp = tmp + 10;

            var tmp_w = this.quest_label.x - (this.quest_label.width / 2);
            let text = this.game.add.text(tmp_w, tmp, route, style_answer);


            text.anchor.set(0.0, 0.5);
            var tmp_i = 0;
            if(i > 0){
                tmp_i = prevText.y;
                tmp_i = tmp_i + (prevText.height / 2);
                tmp_i = tmp_i + 25;
                prevText = text;

                text.y = tmp_i;
            }
            else{
                prevText = text;
            }



            text.setShadow(5,5,'rgba(0,0,0,0.5)',15);

            text.inputEnabled = true;
            text.link = link;

            text.events.onInputDown.add(
                () => {

                    console.log("link:" + link);
                    //console.log("chartext:" + char_text);
                    this.clearTexts();
                    this.stateText = link;


                    if(link.indexOf("John") !== -1){
                        this.character.loadTexture('john');
                        console.log("John");
                            //= this.game.add.image(5,5,'john');
                    }else if(link.indexOf("creepygirl") !== -1){
                        this.character.loadTexture('girl');
                        console.log("Girl");
                            //= this.game.add.image(5,5,'girl');
                    }else if(link.indexOf("Elf") !== -1){
                        this.character.loadTexture('elf');
                        console.log("Elf");
                            //= this.game.add.image(5,5,'elf');
                    }else if(link.indexOf("GoodEnd") !== -1){

                    }

                    this.character.scale.setTo(0.5);
                    this.createTexts();

                }, this);

            this.texts.push(text);
        }

    },

    clearTexts: function(){
        for(var i = 0; i < this.texts.length; i++){
            this.texts[i].destroy();
        }
        this.texts = [];
        this.quest_label.destroy();
    },

    clickText: function(){
        console.log("Hello Again" );
    }
}