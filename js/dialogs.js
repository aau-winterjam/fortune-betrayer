var Dialogs = {
    'John Smith': {
        Start: {
            text: "“Well, you see… I am in quite a predicament… My parents want me to pursue an academic career after school, but… Honestly, I aspire to be a star quarterback.”",
            answers: [
                {text: "“Surely, you have the body for it… Suited for a sports field, I’d say… Or, at least, a cover.”", next: "P2"},
                {text: "“Must be difficult for you... So, how’s school anyway?”", next: "N2"},
                {text: "“Ah, yes… A football career, of course… But only if you were to obtain salmon or pink shorts.”", next: "NE2"}
            ]
        },

        NE2: {
            text: "“Salmon or pink… Was that supposed to be a joke? Hey, I’m really looking for guidance here…”",
            answers: [
                {text: "“Heh, sorry, couldn’t resist. Your spirit seems to be keener on sports, it shows in your posture!”", next: "N3"},
                {text: "“The color of your shorts can affect your future, you know.”", next: "NE3"},
                {text: "“No, wait! The spirits talk to me and say… Watch out from the football when the Robot Unicorn jumps over the moon!”", next: "NE3"}
            ]
        },

        NE3: {
            text: "“That’s… Not even proper advice! I’m not here to listen to fairytales!”",
            answers: [
                {text: "“There are things that we cannot perceive with our eyes alone, you have to believe… But yeah, let’s leave out the unicorns.”", next: "N4"},
                {text: "“Proper advice, yes… So, realadvice or something that makes you happy?", next: "NE4"},
                {text: "“You… Do realize that you’re talking to a fortune teller here?”", next: "NE5"}
            ]
        },

        NE4: {
            text: "“I thought that you’d take me seriously, at least! I just don’t know what to do!”",
            answers: [
                {text: "“Alright, enough fun and games. Let’s challenge the stars and see if you are meant to be one yourself!”", next: "N5"},
                {text: "“Take what seriously? You just gotta man up and decide here! Stop being a whiny brat, geez…”", next: "NE5"},
            ]
        },

        NE4: {
            text: "“You know what?! We’re done here! Take that crystal ball and shove it up your--!! Argh!”",
        },


        N2: {
            text: "Huh… School’s fine, I guess… My parents say that I should drop my hobby. Because, you know, my dream is impossible or whatever…”",
            answers: [
                {text: "“Impossible is just a word thrown around by people that are afraid of the future! Fate wants you to tackle the world head on!”", next: "P3"},
                {text: " “To drop school wouldn’t be the right path either! What is it that you strive for, at heart?”", next: "N3"},
                {text: "As I see it, neither of those things are what you really want! The confusion is caused by your inner turmoil that keeps you from being an accountant!”", next: "NE3"}
            ]
        },


        N3: {
            text: " “In the end, I just… I want to follow my dreams, but this is a big decision… I’m lost.”",
            answers: [
                {text: "“And we will find a light! You, me and the great cosmos! Something like that! Magic sparkles and stuff! Wouldn’t that be ideal?”", next: "P4"},
                {text: "Then let’s put that decision to the test! Because that is what you are here for, correct?”", next: "N4"},
                {text: "“You really want to put your fate in my hands? Out of all people, a stranger in purple robes hands you the magic key?”", next: "NE4"}
            ]
        },


        N4: {
            text: "“Obviously… I wasn’t exactly expecting a tea ceremony.”",
            answers: [
                {text: "“Don’t tempt me or I’ll charge you for that service! But no, we’ll go for the stars instead! What better than the sky to tell you your fate!", next: "P2"},
                {text: "“I could put on a kettle while we check what… The sky has to say about it? ", next: "N2"},
                {text: "“I don’t see how a tea ceremony wouldn’t help you. Sit it out and start drinking... Some tea, I mean.”", next: "NE2"}
            ]
        },


        N5: {
            text: " “All right… Please just find a solution for my troubles…”",
        },

         P2: {
                    text: "“Wow! Y-You really think so? Can you feel it in my… Sphere or something?”",
                    answers: [
                        {text: "“Of course! What else would it be? The energies in your soul are… Overflowing!”", next: "P3"},
                        {text: "“Sphere? Ah, yes… The sphere waits for you to act, but you hold yourself back.”", next: "N3"},
                        {text: "“The what? Oh! Uhm… Yeah no, that was kind of obvious to me to understand.”", next: "NE3"}
                    ]
                },

         P3: {
                    text: "“That’s amazing! So, my potential is so great that I could be a Superstar?”",
                    answers: [
                        {text: "“Well, yes, you could! The Germans even have a word for dreamers like you, it’s Wunschdenken.”", next: "P4"},
                        {text: "“That I can’t promise you… At least, I’m not the one to tell you if you can make it or not.”", next: "N4"},
                        {text: "·  “Let’s not get ahead of ourselves… There’s a lot of work waiting you.”", next: "NE4"}
                    ]
                },

         P4: {
                    text: "“Fascinating… And is that what you can see for me? A good outcome?”",
                    answers: [
                        {text: "“You want me to find out? One glimpse into my crystal ball and the stars will reveal the truth to me… Of the possible Superstar that you could become!”", next: "P5"},
                        {text: "“Whatever it may be… That is what we must find out, so that you may choose the right path.”", next: "N5"},
                    ]
                },
         P5: {
                    text: "“Indeed! I can’t wait for the results of this!”",

                },
    },

    'Elf Dude': {
        Start: {
            text: "“I don't know where to start... As you might know, I am the true King of you and all the people in this realm. But they became ignorant and began denying their true ruler!”",
            answers: [
                {text: "““My Lord, it is a pleasure to serve you. I am upset to hear that the people won't recognize your greatness.”", next: "P2"},
                {text: "“Let's see how I can help you... You seem stressed about your future. Maybe you take a step back and look after yourself?”", next: "N2"},
                {text: "“Don't get too full of yourself. People would just rather follow stronger leaders after all!”", next: "NE2"}
            ]
        },

        NE2: {
            text: "“Good grief! I think, you don't know who you're talking to!”",
            answers: [
                {text: "“Please forgive my inappropriate behavior! I didn't recognize your royal face and your struggles at first.”", next: "N3"},
                {text: "“Maybe you should introduce yourself better to me, I think.”", next: "NE3"},
                {text: "“Perhaps you should talking to your dancing mushrooms first before I can take you seriously.”", next: "NE3"}
            ]
        },

        NE3: {
            text: "“You should do what you're here for and stop this non-sense! Otherwise, I'll have you publicly executed in front of my people!”",
            answers: [
                {text: "“Please have mercy! I am just a small peasant in your presence. I pledge loyalty to the crown!”", next: "N4"},
                {text: "“Chill... So, what is it that you want me to do anyway?“", next: "NE4"},
                {text: "“If your people are as imaginary as your power is, I wouldn't mind! Whether it is in here or 'over there', that would be the same.”", next: "NE5"}
            ]
        },

        NE4: {
            text: "“Do you value your life in any shape or form? You can annoy the worms with your fortune telling skills soon...”",
            answers: [
                {text: "“That would be a shame indeed... I would prefer a proper conversation with you.”", next: "N5"},
                {text: "“Ah, now I get where the resemblance comes from... I can tell your slimy relatives what you're doing up there. You being a 'true highborn', of course.”", next: "NE5"},
            ]
        },

        NE4: {
            text: "“I don't think you understand the situation that you brought yourself into. Start praying and you might live another night. We're done! Farewell...”",
        },


        N2: {
            text: "“What gives you the right to teach me? I know best what to do in this situation. You just need to listen to me.”",
            answers: [
                {text: "“Forgive me, my Lord. I'm just worried about your well-being. I exist only to serve you, of course...”", next: "P3"},
                {text: "“Well, sorry, if you were to explain it to me, I might understand better!”", next: "N3"},
                {text: "“You know best how to ruin a conversation, I think...”", next: "NE3"}
            ]
        },


        N3: {
            text: "“I suppose, your simple mind is not capable of grasping the situation that we're in... So, let me put it like this, I am the rightful King, but the others won't see so.”",
            answers: [
                {text: "“I will do anything to be in your favor, your Highness May I ask for permission?”", next: "P4"},
                {text: "“Sometimes you need more distance to see the bigger picture.”", next: "N4"},
                {text: "“But at least I'm capable of grasping reality! Were you dropped as a baby too often?”", next: "NE4"}
            ]
        },


        N4: {
            text: "“I am aware of the solutions. Spare me with your wisdom... My safety is at danger, so I need to be careful with these ignorant fiends. Will you obey me then, woman?”",
            answers: [
                {text: "“I will bow to the Lord of the realm! Please accept my apology...”", next: "P2"},
                {text: "“While I think that you are in need of some rest, I suppose that I will be of service either way...”", next: "N2"},
                {text: "“Just relax already, man. Maybe take off those fake pointy ears and get real”", next: "NE2"}
            ]
        },


        N5: {
            text: "“As ignorant as you might sound, I will stay and listen to whatever you might have to say.”",
        },

         P2: {
            text: "“I am disappointed of the mental capacity of some people. If they don't listen to me and my orders, they'll surely be doomed!”",
            answers: [
            {text: "“I agree! There are limitations to a mind of a commoner, your Highness. I doubt you can compare them to you.”", next: "P3"},
            {text: "“I understand your troubles. Sometimes you can't do the best for everyone.”", next: "N3"},
            {text: "“Maybe you suffer from delusion of grandeur?”", next: "NE3"}
                    ]
            },

         P3: {
            text: "“At least, I am talking to one of the few sane people in this realm. I wish, I could find a way to erase the others...”",
            answers: [
            {text: "“If it is your wish, I will look into the cards and tell you the future, your Grace.”", next: "P4"},
            {text: "“It is always important to be a step ahead to achieve your goal, I guess.”", next: "N4"},
            {text: "“Sometimes it is better to stop taking pills... Your hallucinations are getting a bit out of hand.”", next: "NE4"}
                    ]
            },

         P4: {
            text: "“I need to be informed about the stupid plans of some wannabe usurper that aims to gain my throne.”",
            answers: [
            {text: "“I am your devote servant, please let me make a small contribution to your greater plan.”", next: "P5"},
            {text: "“I might be afraid of the outcome, but if that is your wish...”", next: "N5"},
                    ]
            },
         P5: {
             text: "“I will grant your wish and give you the pleasure to serve me.”",

             },
    },

    Ghost: {
            P1: {
                text: "“S-Sorry to interrupt… I wondered if you could help me out a bit…”",
                answers: [
                    {text: "“S-Sure! There’s… Nothing else that I’d rather do for… A-A spirit…?”", next: "P2"},
                    {text: "“Uhm. Question. What the heck are you?”", next: "N2"},
                    {text: "“BegONE, FIEND!!”", next: "NE2"}
                ]
            },

            P2: {
                text: "“Thank you… It’s just… I’ve passed away recently and I am too scared to find out… Where I’ll end up, you know…”",
                answers: [
                    {text: "“And I’m… Supposed to help you out with that?”", next: "P3"},
                    {text: "“How do you even know that there’s… Somewhere for you to go?”", next: "N3"},
                    {text: "“You know, right now I wonder if I inhaled too much of the incense…”", next: "NE3"}
                ]
            },

            P3: {
                text: "“Well, yeah! I mean… You can see me and… Use fortune telling and… Guide my way?”",
                answers: [
                     {text: "“For a ghost, you really aren’t all that scary… I guess that it is scary enough to not know where you’re going. If it’s really going to be like this when you die, then that’s sad.”", next: "P4"},
                     {text: "“I don’t know nothing about ghosts, so I wouldn’t know how I can judge that… Am I supposed to listen to your life story or…?”", next: "N4"},
                     {text: "“Do I look like a priest or something? I don’t count sins and whatever! That’s not what I could be doing…”", next: "NE4"}
                ]
            },

            P4: {
                text: "“Does… Does that mean that you will really help me?”",
                answers: [
                     {text: "“Look. Let’s be real here. I don’t have any powers. Seeing you is messing with my mind. But I’ll roll with it, because I feel bad for you whether you are a hallucination or not.”", next: "P5"},
                     {text: "“Help you? Y-Yeah… Sure… That’s what I’ll be doing. Because that’s what I do. Help people with things and stuff.”", next: "NE5"}
                ]
            },

            P5: {
                text: "“Goodness… T-Thank you so much! Sniff… I’ll put in a good word for you when I’m gone…”",
            },

            N2: {
                text: "“Me? Uh… I’m a person…? At least, I was… Now I’m very much dead and… Well…”",
                answers: [
                       {text: "“Not feeling so good? Need some cheering up? I can’t resurrect you, but could… Assist you?”", next: "P3"},
                       {text: "“How long have you been… Like this?”", next: "N3"},
                       {text: "“And what am I supposed to do? I’m a fortune teller and not a necromancer!” ", next: "NE3"}
                ]
            },

            N3: {
                text: "“I’ve gotten… Into an accident with my bike a while back… Not that I remember what exactly happened… Just found myself like this.”",
                answers: [
                       {text: "“At this point, I don’t even question anything anymore. It’s too late for that and this might as well be a dream, so what do you need?”", next: "P4"},
                       {text: "“A bike accident killed you? I wonder how you managed to get that far, but you know what? I’d rather not know…”", next: "N4"},
                       {text: "“It’s not exactly natural for you to end up as a ghost in the world of the living… Shouldn’t you just shoo off?”", next: "NE4"}
                ]
            },

            N4: {
                text: "“N-Not that it matters a whole lot now… So… Can you tell me if I’ll go Heaven or Hell?”",
                answers: [
                       {text: "“You mean, I can be the deciding factor in this?! Wow… That’s almost like having the power of God… Let’s do this, buddy! ”", next: "P5"},
                       {text: "“I don’t know you, so I can’t determine that… But I can give it a try.”", next: "N5"},
                       {text: "“W-Woah, wait a minute! Why am I supposed to decide that? I can’t do that, you should just go and find out yourself!”", next: "NE4"}
                ]
            },

            N5: {
                text: "“Thank you… T-That is all that I would ask for…”",
            },

            NE2: {
                text: "“Wow… That was rude… I just wanted to ask for help, no reason to be mean…”",
                answers: [
                       {text: "“I’m sorry, knee-jerk reaction… I’m not used to seeing dead people.”", next: "N3"},
                       {text: "“Can you really blame me?! You just… Barge in like that a-and I almost had a heart attack!”", next: "NE3"},
                       {text: "“YOU CANNOT TRICK ME!”", next: "NE3"}
                ]
            },

            NE3: {
                text: "“There’s n-no need for that tone you know… Do I really look like I would hurt you?”",
                answers: [
                       {text: "“Honestly… I don’t know what ghosts can do… So, yeah, I’m worried, but I’ll try my best to keep it together.”", next: "N4"},
                       {text: "“Are you expecting me to trust you? Ghosts don’t walk the Earth, that’s not… Normal, you know.”", next: "NE4"},
                       {text: "“If you won’t get away from me, I’ll call the GhostbustersTM on you!!”", next: "NE4"}
                ]
            },

            NE4: {
                text: "“Sniff… All I want is… To know if I’m going to be okay or not… Is that too much to ask for?”",
                answers: [
                       {text: "“You know what? We’ll… Try to figure something out for you, okay? But I… Won’t promise anything…”", next: "N5"},,
                       {text: "“AAAAAAAAAAAAAAAAAAAAAAAAA”", next: "NE5"}
                ]
            },

            NE5: {
                 text: "“I… didn’t think that you’d be so mean… Sniff… One day, we’ll meet again…”",
            }
        }
};
